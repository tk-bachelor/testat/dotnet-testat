﻿using AutoReservation.Common.DataTransferObjects.Core;
using System.Runtime.Serialization;
using System.Text;

namespace AutoReservation.Common.DataTransferObjects
{
    [DataContract]
    public class AutoDto : DtoBase<AutoDto>
    {
        // private fields
        private int id;
        private string marke;
        private int basistarif;
        private int tagestarif;
        private AutoKlasse autoklasse;
        private byte[] rowVersion;

        // properties
        [DataMember]
        public int Id
        {
            get { return id; }
            set
            {
                if (id == value)
                {
                    return;
                }
                id = value;
                this.OnPropertyChanged(nameof(Id));
            }
        }

        [DataMember]
        public string Marke
        {
            get { return marke; }
            set
            {
                if (marke == value)
                {
                    return;
                }
                marke = value;
                this.OnPropertyChanged(nameof(Marke));
            }
        }
        [DataMember]
        public int Basistarif
        {
            get { return basistarif; }
            set
            {
                if (basistarif == value)
                {
                    return;
                }
                basistarif = value;
                this.OnPropertyChanged(nameof(Basistarif));
            }
        }
        [DataMember]
        public int Tagestarif
        {
            get { return tagestarif; }
            set
            {
                if (tagestarif == value)
                {
                    return;
                }
                tagestarif = value;
                this.OnPropertyChanged(nameof(Tagestarif));
            }
        }
        [DataMember]
        public AutoKlasse AutoKlasse
        {
            get { return autoklasse; }
            set
            {
                if (autoklasse == value)
                {
                    return;
                }
                autoklasse = value;
                this.OnPropertyChanged(nameof(AutoKlasse));
            }
        }

        [DataMember]
        public byte[] RowVersion
        {
            get { return rowVersion; }
            set
            {
                if(rowVersion == value)
                {
                    return;
                }
                rowVersion = value;
                this.OnPropertyChanged(nameof(RowVersion));
            }
        }


        public override string Validate()
        {
            StringBuilder error = new StringBuilder();
            if (string.IsNullOrEmpty(marke))
            {
                error.AppendLine("- Marke ist nicht gesetzt.");
            }
            if (tagestarif <= 0)
            {
                error.AppendLine("- Tagestarif muss grösser als 0 sein.");
            }
            if (AutoKlasse == AutoKlasse.Luxusklasse && basistarif <= 0)
            {
                error.AppendLine("- Basistarif eines Luxusautos muss grösser als 0 sein.");
            }

            if (error.Length == 0) { return null; }

            return error.ToString();
        }

        public override string ToString()
            => $"{Id}; {Marke}; {Tagestarif}; {Basistarif}; {AutoKlasse}";

    }
}
