﻿using AutoReservation.Common.DataTransferObjects.Core;
using System;
using System.Runtime.Serialization;
using System.Text;

namespace AutoReservation.Common.DataTransferObjects
{
    [DataContract]
    public class ReservationDto : DtoBase<ReservationDto>
    {
        // private fields
        private int reservationsNr;
        private DateTime von;
        private DateTime bis;
        private AutoDto auto;
        private KundeDto kunde;
        private byte[] rowVersion;

        // properties
        [DataMember]

        public int ReservationsNr
        {
            get { return reservationsNr; }
            set
            {
                if (reservationsNr == value)
                {
                    return;
                }
                reservationsNr = value;
                this.OnPropertyChanged(nameof(ReservationsNr));
            }
        }

        [DataMember]
        public DateTime Von
        {
            get { return von; }
            set
            {
                if (von == value)
                {
                    return;
                }
                von = value;
                this.OnPropertyChanged(nameof(Von));
            }
        }

        [DataMember]
        public DateTime Bis
        {
            get { return bis; }
            set
            {
                if (bis == value)
                {
                    return;
                }
                bis = value;
                this.OnPropertyChanged(nameof(Bis));
            }
        }


        [DataMember]
        public AutoDto Auto
        {
            get { return auto; }
            set
            {
                if (auto == value)
                {
                    return;
                }
                auto = value;
                this.OnPropertyChanged(nameof(Auto));
            }
        }


        [DataMember]
        public KundeDto Kunde
        {
            get { return kunde; }
            set
            {
                if (kunde == value)
                {
                    return;
                }
                kunde = value;
                this.OnPropertyChanged(nameof(Kunde));
            }
        }
        [DataMember]
        public byte[] RowVersion
        {
            get { return rowVersion; }
            set
            {
                if (rowVersion == value)
                {
                    return;
                }
                rowVersion = value;
                this.OnPropertyChanged(nameof(RowVersion));
            }
        }


        public override string Validate()
        {
            StringBuilder error = new StringBuilder();
            if (Von == DateTime.MinValue)
            {
                error.AppendLine("- Von-Datum ist nicht gesetzt.");
            }
            if (Bis == DateTime.MinValue)
            {
                error.AppendLine("- Bis-Datum ist nicht gesetzt.");
            }
            if (Von > Bis)
            {
                error.AppendLine("- Von-Datum ist grösser als Bis-Datum.");
            }
            if (Auto == null)
            {
                error.AppendLine("- Auto ist nicht zugewiesen.");
            }
            else
            {
                string autoError = Auto.Validate();
                if (!string.IsNullOrEmpty(autoError))
                {
                    error.AppendLine(autoError);
                }
            }
            if (Kunde == null)
            {
                error.AppendLine("- Kunde ist nicht zugewiesen.");
            }
            else
            {
                string kundeError = Kunde.Validate();
                if (!string.IsNullOrEmpty(kundeError))
                {
                    error.AppendLine(kundeError);
                }
            }

            if (error.Length == 0) { return null; }

            return error.ToString();
        }

        public override string ToString()
            => $"{ReservationsNr}; {Von}; {Bis}; {Auto}; {Kunde}";

    }
}
