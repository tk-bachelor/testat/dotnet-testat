﻿using AutoReservation.Common.DataTransferObjects.Core;
using System;
using System.Runtime.Serialization;
using System.Text;

namespace AutoReservation.Common.DataTransferObjects
{
    [DataContract]
    public class KundeDto : DtoBase<KundeDto>
    {
        // private fields
        private int id;
        private string vorname;
        private string nachname;
        private DateTime geburtsdatum;
        private byte[] rowVersion;

        // property fields
        [DataMember]
        public int Id
        {
            get
            { return id; }
            set
            {
                if (id == value)
                {
                    return;
                }
                id = value;
                this.OnPropertyChanged(nameof(Id));
            }
        }

        [DataMember]
        public string Vorname
        {
            get { return vorname; }
            set
            {
                if (vorname == value)
                {
                    return;
                }
                vorname = value;
                this.OnPropertyChanged(nameof(Vorname));
            }
        }

        [DataMember]
        public string Nachname
        {
            get { return nachname; }
            set
            {
                if (nachname == value)
                {
                    return;
                }
                nachname = value;
                this.OnPropertyChanged(nameof(Nachname));
            }
        }

        [DataMember]
        public DateTime Geburtsdatum
        {
            get { return geburtsdatum; }
            set
            {
                if (geburtsdatum == value)
                {
                    return;
                }
                geburtsdatum = value;
                this.OnPropertyChanged(nameof(Geburtsdatum));
            }
        }

        [DataMember]
        public byte[] RowVersion
        {
            get { return rowVersion; }
            set
            {
                if (rowVersion == value)
                {
                    return;
                }
                rowVersion = value;
                this.OnPropertyChanged(nameof(RowVersion));
            }
        }

        public override string Validate()
        {
            StringBuilder error = new StringBuilder();
            if (string.IsNullOrEmpty(Nachname))
            {
                error.AppendLine("- Nachname ist nicht gesetzt.");
            }
            if (string.IsNullOrEmpty(Vorname))
            {
                error.AppendLine("- Vorname ist nicht gesetzt.");
            }
            if (Geburtsdatum == DateTime.MinValue)
            {
                error.AppendLine("- Geburtsdatum ist nicht gesetzt.");
            }

            if (error.Length == 0) { return null; }

            return error.ToString();
        }

        public override string ToString()
            => $"{Id}; {Nachname}; {Vorname}; {Geburtsdatum}";

    }
}
