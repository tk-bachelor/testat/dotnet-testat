﻿using AutoReservation.Common.DataTransferObjects;
using System.Collections.Generic;
using System.ServiceModel;

namespace AutoReservation.Common.Interfaces
{
    [ServiceContract]
    public interface IAutoReservationService
    {
        // Auto CRUD operations
        [OperationContract]
        List<AutoDto> GetAutos();
        [OperationContract]
        AutoDto GetAutoById(int id);
        [OperationContract]
        int InsertAuto(AutoDto auto);
        [OperationContract]
        [FaultContract(typeof(AutoDto))]
        void UpdateAuto(AutoDto auto);
        [OperationContract]
        void DeleteAuto(AutoDto auto);

        // Kunde CRUD operations
        [OperationContract]
        List<KundeDto> GetKunden();
        [OperationContract]
        KundeDto GetKundeById(int id);
        [OperationContract]
        int InsertKunde(KundeDto kunde);
        [OperationContract]
        [FaultContract(typeof(KundeDto))]
        void UpdateKunde(KundeDto kunde);
        [OperationContract]
        void DeleteKunde(KundeDto kunde);

        // Reservation CRUD operations
        [OperationContract]
        List<ReservationDto> GetReservationen();
        [OperationContract]
        ReservationDto GetReservationById(int id);
        [OperationContract]
        int InsertReservation(ReservationDto reservation);
        [OperationContract]
        [FaultContract(typeof(ReservationDto))]
        void UpdateReservation(ReservationDto reservation);
        [OperationContract]
        void DeleteReservation(ReservationDto reservation);
    }
}
