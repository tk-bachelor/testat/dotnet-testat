﻿using AutoReservation.Common.DataTransferObjects;
using AutoReservation.Common.Interfaces;
using AutoReservation.TestEnvironment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace AutoReservation.Service.Wcf.Testing
{
    [TestClass]
    public abstract class ServiceTestBase
    {
        protected abstract IAutoReservationService Target { get; }

        [TestInitialize]
        public void InitializeTestData()
        {
            TestEnvironmentHelper.InitializeTestData();
        }

        #region Read all entities

        [TestMethod]
        public void GetAutosTest()
        {
            Assert.IsNotNull(Target.GetAutos());
        }

        [TestMethod]
        public void GetKundenTest()
        {
            Assert.IsTrue(Target.GetKunden().Count == 4);
        }

        [TestMethod]
        public void GetReservationenTest()
        {
            Assert.IsTrue(Target.GetReservationen().Count == 3);
        }

        #endregion

        #region Get by existing ID

        [TestMethod]
        public void GetAutoByIdTest()
        {
            Assert.IsInstanceOfType(Target.GetAutoById(1), typeof(AutoDto));
        }

        [TestMethod]
        public void GetKundeByIdTest()
        {
            Assert.IsNotNull(Target.GetAutoById(1));
        }

        [TestMethod]
        public void GetReservationByNrTest()
        {
            Assert.AreEqual(Target.GetReservationById(1).Auto.Id, 1);
        }

        #endregion

        #region Get by not existing ID

        [TestMethod]
        public void GetAutoByIdWithIllegalIdTest()
        {
            Assert.IsNull(Target.GetAutoById(99));
        }

        [TestMethod]
        public void GetKundeByIdWithIllegalIdTest()
        {
            Assert.IsNull(Target.GetKundeById(99));
        }

        [TestMethod]
        public void GetReservationByNrWithIllegalIdTest()
        {
            Assert.IsNull(Target.GetReservationById(99));
        }

        #endregion

        #region Insert

        [TestMethod]
        public void InsertAutoTest()
        {
            AutoDto auto = new AutoDto();
            auto.AutoKlasse = AutoKlasse.Luxusklasse;
            auto.Basistarif = 120;
            auto.Marke = "Ferrari";
            auto.Tagestarif = 300;

            int id = Target.InsertAuto(auto);
            AutoDto foundAuto = Target.GetAutoById(id);
            Assert.AreEqual(foundAuto.Marke, auto.Marke);
        }

        [TestMethod]
        public void InsertKundeTest()
        {
            KundeDto kunde = new KundeDto { Vorname = "Keerthikan", Nachname = "Thurai", Geburtsdatum = new DateTime(1991, 11, 14) };
            int id = Target.InsertKunde(kunde);
            Assert.IsNotNull(id); // is it nullable?
            Assert.AreEqual(id, 5);
        }

        [TestMethod]
        public void InsertReservationTest()
        {
            ReservationDto reservation = new ReservationDto();
            reservation.Von = DateTime.Now;
            reservation.Bis = DateTime.Now.AddDays(2);
            reservation.Auto = Target.GetAutoById(2);
            reservation.Kunde = Target.GetKundeById(4);

            Assert.AreEqual(reservation.Kunde.Vorname, "Rainer");
            int id = Target.InsertReservation(reservation);

            ReservationDto reservationAdded = Target.GetReservationById(id);
            Assert.IsNotNull(reservationAdded.Auto);
        }

        #endregion

        #region Delete  

        [TestMethod]
        public void DeleteAutoTest()
        {
            Target.DeleteAuto(Target.GetAutoById(1));
            Assert.IsNull(Target.GetAutoById(1));
        }

        [TestMethod]
        public void DeleteKundeTest()
        {
            KundeDto kunde = Target.GetKundeById(1);
            Target.DeleteKunde(kunde);

            Assert.IsNull(Target.GetKundeById(1));
        }

        [TestMethod]
        public void DeleteReservationTest()
        {
            ReservationDto reservation = Target.GetReservationById(1);
            Target.DeleteReservation(reservation);

            Assert.IsNull(Target.GetReservationById(1));
            Assert.IsNotNull(Target.GetAutoById(reservation.Auto.Id));
            Assert.AreEqual(reservation.Kunde.Id, Target.GetKundeById(reservation.Kunde.Id).Id);
        }

        #endregion

        #region Update

        [TestMethod]
        public void UpdateAutoTest()
        {
            AutoDto auto = Target.GetAutoById(2);
            auto.Marke = "Porsche";
            Target.UpdateAuto(auto);

            Assert.AreEqual("Porsche", Target.GetAutoById(2).Marke);
        }

        [TestMethod]
        public void UpdateKundeTest()
        {
            KundeDto kunde = Target.GetKundeById(3);
            kunde.Nachname = "Stark";
            Target.UpdateKunde(kunde);

            Assert.AreEqual("Stark", Target.GetKundeById(3).Nachname);
        }

        [TestMethod]
        public void UpdateReservationTest()
        {
            ReservationDto reservation = Target.GetReservationById(1);
            AutoDto auto = Target.GetAutoById(2);
            Assert.AreNotEqual(reservation.Auto.Id, auto.Id);

            reservation.Auto = auto;
            Target.UpdateReservation(reservation);
            Assert.AreEqual(reservation.Auto.Id, auto.Id);
        }

        #endregion

        #region Update with optimistic concurrency violation

        [TestMethod]

        public void UpdateAutoWithOptimisticConcurrencyTest()
        {
            AutoDto a1 = Target.GetAutoById(1);
            AutoDto a2 = Target.GetAutoById(1);

            try
            {
                a1.Tagestarif = 120;
                Target.UpdateAuto(a1);

                a2.Tagestarif = 130;
                Target.UpdateAuto(a2);
                Assert.Fail("LocalOptimisticConcurrencyException expected");
            }
            catch (FaultException<AutoDto> ex)
            {
                Assert.AreEqual(a1.Tagestarif, ex.Detail.Tagestarif);
                throw;
            }
            catch (Exception ex)
            {
                Assert.Fail("Wrong Exception");
            }
        }

        [TestMethod]
        public void UpdateKundeWithOptimisticConcurrencyTest()
        {
            KundeDto a1 = Target.GetKundeById(1);
            KundeDto a2 = Target.GetKundeById(1);

            try
            {
                a1.Vorname = "Keerthikan";
                Target.UpdateKunde(a1);

                a2.Vorname = "Tony";
                Target.UpdateKunde(a2);
                Assert.Fail("LocalOptimisticConcurrencyException expected");
            }
            catch (FaultException<KundeDto> ex)
            {
                Assert.AreEqual(a1.Vorname, ex.Detail.Vorname);
            }
            catch (FaultException ex)
            {
                Assert.IsTrue(ex.Message.Contains("Kunde"));
            }
            catch (Exception ex)
            {
                Assert.Fail("Wrong Exception");
            }
        }

        [TestMethod]
        public void UpdateReservationWithOptimisticConcurrencyTest()
        {
            ReservationDto a1 = Target.GetReservationById(1);
            ReservationDto a2 = Target.GetReservationById(1);

            try
            {
                a1.Von = DateTime.Now;
                Target.UpdateReservation(a1);

                a2.Von = DateTime.Now.AddDays(2);
                Target.UpdateReservation(a2);
                Assert.Fail("LocalOptimisticConcurrencyException expected");
            }
            catch (FaultException<ReservationDto> ex)
            {
                Assert.AreEqual(a1.Von.ToString(), ex.Detail.Von.ToString());
            }
            catch (FaultException ex)
            {
                Assert.IsTrue(ex.Message.Contains("Reservation"));
            }
            catch (Exception ex)
            {
                Assert.Fail("Wrong Exception");
            }
        }

        #endregion
    }
}
