﻿using AutoReservation.Common.Interfaces;
using System;
using System.Diagnostics;
using AutoReservation.Common.DataTransferObjects;
using System.Collections.Generic;
using AutoReservation.BusinessLayer;
using AutoReservation.Dal.Entities;
using System.ServiceModel;

namespace AutoReservation.Service.Wcf
{
    public class AutoReservationService : IAutoReservationService
    {
        private AutoReservationBusinessComponent businessComponent;

        public AutoReservationService()
        {
            this.businessComponent = new AutoReservationBusinessComponent();
        }

        // Auto methods
        public List<AutoDto> GetAutos()
        {
            WriteActualMethod();
            return businessComponent.GetAutos().ConvertToDtos();
        }

        public void DeleteAuto(AutoDto auto)
        {
            WriteActualMethod();
            businessComponent.DeleteAuto(auto.ConvertToEntity()); 
        }

        public AutoDto GetAutoById(int id)
        {
            WriteActualMethod();
            return businessComponent.GetAutoById(id).ConvertToDto();
        }

        public int InsertAuto(AutoDto auto)
        {
            WriteActualMethod();
            return businessComponent.InsertAuto(auto.ConvertToEntity());
        }

        public void UpdateAuto(AutoDto auto)
        {
            try
            {
                WriteActualMethod();
                businessComponent.UpdateAuto(auto.ConvertToEntity());
            }catch(LocalOptimisticConcurrencyException<Auto> ex)
            {
                throw new FaultException<AutoDto>(ex.MergedEntity.ConvertToDto(), ex.Message);
            }
        }

        // Kunden methods
        public KundeDto GetKundeById(int id)
        {
            WriteActualMethod();
            return businessComponent.GetKundeById(id).ConvertToDto();
        }

        public List<KundeDto> GetKunden()
        {
            WriteActualMethod();
            return businessComponent.GetKunden().ConvertToDtos();
        }

        public void DeleteKunde(KundeDto kunde)
        {
            WriteActualMethod();
            businessComponent.DeleteKunde(kunde.ConvertToEntity());
        }

        public int InsertKunde(KundeDto kunde)
        {
            WriteActualMethod();
            return businessComponent.InsertKunde(kunde.ConvertToEntity());
        }

        public void UpdateKunde(KundeDto kunde)
        {
            try
            {
                WriteActualMethod();
                businessComponent.UpdateKunde(kunde.ConvertToEntity());
            }
            catch (LocalOptimisticConcurrencyException<Kunde> ex)
            {
                throw new FaultException<KundeDto>(ex.MergedEntity.ConvertToDto(), ex.Message);
            }
        }

        // Reservation methods

        public void DeleteReservation(ReservationDto reservation)
        {
            WriteActualMethod();
            businessComponent.DeleteReservation(reservation.ConvertToEntity());
        }
      
        public ReservationDto GetReservationById(int id)
        {
            WriteActualMethod();
            return businessComponent.GetReservationById(id).ConvertToDto();
        }

        public List<ReservationDto> GetReservationen()
        {
            WriteActualMethod();
            return businessComponent.GetReservationen().ConvertToDtos();
        }   

        public int InsertReservation(ReservationDto reservation)
        {
            WriteActualMethod();
            return businessComponent.InsertReservation(reservation.ConvertToEntity());
        }

        public void UpdateReservation(ReservationDto reservation)
        {
            try
            {
                WriteActualMethod();
                businessComponent.UpdateReservation(reservation.ConvertToEntity());
            }
            catch (LocalOptimisticConcurrencyException<Reservation> ex)
            {
                throw new FaultException<ReservationDto>(ex.MergedEntity.ConvertToDto(), ex.Message);
            }
        }

        private static void WriteActualMethod()
        {
            Console.WriteLine($"Calling: {new StackTrace().GetFrame(1).GetMethod().Name}");
        }
    }
}