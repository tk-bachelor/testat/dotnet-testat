using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace AutoReservation.Dal.Entities
{
    [Table("Auto")]
    public abstract class Auto
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(20)]
        public string Marke { get; set; }
        [Required]
        public int Tagestarif { get; set; }

        //[Required]
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual List<Reservation> Reservationen { get; set; }
    }

    public class LuxusklasseAuto : Auto
    {
        [Required]
        public int Basistarif { get; set; }
    }

    public class MittelklasseAuto : Auto
    {

    }

    public class StandardAuto : Auto
    {

    }
}
