﻿using System;
using System.Collections.Generic;
using AutoReservation.Common.DataTransferObjects;
using AutoReservation.Common.Interfaces;

namespace AutoReservation.Ui.Factory
{
    public class NullServiceFactory : IServiceFactory
    {
        public IAutoReservationService GetService()
        {
            return new NullAutoReservationService();
        }
    }

    public class NullAutoReservationService : IAutoReservationService
    {
        public void DeleteAuto(AutoDto auto)
        {
            throw new NotImplementedException();
        }

        public void DeleteKunde(KundeDto kunde)
        {
            throw new NotImplementedException();
        }

        public void DeleteReservation(ReservationDto reservation)
        {
            throw new NotImplementedException();
        }

        public AutoDto GetAutoById(int id)
        {
            throw new NotImplementedException();
        }

        public List<AutoDto> GetAutos()
        {
            throw new NotImplementedException();
        }

        public KundeDto GetKundeById(int id)
        {
            throw new NotImplementedException();
        }

        public List<KundeDto> GetKunden()
        {
            throw new NotImplementedException();
        }

        public ReservationDto GetReservationById(int id)
        {
            throw new NotImplementedException();
        }

        public List<ReservationDto> GetReservationen()
        {
            throw new NotImplementedException();
        }

        public int InsertAuto(AutoDto auto)
        {
            throw new NotImplementedException();
        }

        public int InsertKunde(KundeDto kunde)
        {
            throw new NotImplementedException();
        }

        public int InsertReservation(ReservationDto reservation)
        {
            throw new NotImplementedException();
        }

        public void UpdateAuto(AutoDto auto)
        {
            throw new NotImplementedException();
        }

        public void UpdateKunde(KundeDto kunde)
        {
            throw new NotImplementedException();
        }

        public void UpdateReservation(ReservationDto reservation)
        {
            throw new NotImplementedException();
        }
    }
}
