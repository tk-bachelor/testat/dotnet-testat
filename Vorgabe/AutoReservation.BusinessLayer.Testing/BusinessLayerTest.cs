﻿using AutoReservation.Dal.Entities;
using AutoReservation.TestEnvironment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace AutoReservation.BusinessLayer.Testing
{
    [TestClass]
    public class BusinessLayerTest
    {

        private AutoReservationBusinessComponent target;
        private AutoReservationBusinessComponent Target
        {
            get
            {
                if (target == null)
                {
                    target = new AutoReservationBusinessComponent();
                }
                return target;
            }
        }

        [TestInitialize]
        public void InitializeTestData()
        {
            TestEnvironmentHelper.InitializeTestData();
        }

        [TestMethod]
        public void UpdateAutoTest()
        {
            string marke = "Ford Focus";
            Auto a = Target.GetAutoById(1);
            a.Marke = "Ford Focus";
            Target.UpdateAuto(a);

            Auto updatedA = Target.GetAutoById(1);
            Assert.AreEqual(updatedA.Marke, marke);
        }

        [TestMethod]
        [ExpectedException(typeof(LocalOptimisticConcurrencyException<Kunde>))]
        public void UpdateKundeTest()
        {
            Kunde a1 = Target.GetKundeById(1);
            Kunde a2 = Target.GetKundeById(1);

            a1.Vorname = "Keerthikan";
            Target.UpdateKunde(a1);

            Kunde a1Updated = Target.GetKundeById(1);
            Assert.AreEqual(a1Updated.Vorname, "Keerthikan");

            try
            {
                a2.Vorname = "Unknown";
                Target.UpdateKunde(a2);
                Assert.Fail("LocalOptimisticConcurrencyException expected");
            }
            catch (LocalOptimisticConcurrencyException<Kunde> ex)
            {
                // exception expected
                Assert.AreEqual("Keerthikan", ex.MergedEntity.Vorname);
                throw;
            }
            catch { Assert.Fail("Wrong Exception"); }
        }

        [TestMethod]
        public void UpdateReservationTest()
        {
            Reservation r = Target.GetReservationById(1);
            Auto b = Target.GetAutoById(2);

            // Reservation should have a different auto
            Assert.AreNotEqual(r.Auto.Id, b.Id);
            r.Auto = b;
            r.AutoId = b.Id;
            Target.UpdateReservation(r);

            r = Target.GetReservationById(1);
            Assert.AreEqual(r.Auto.Id, b.Id);
        }
    }

}
