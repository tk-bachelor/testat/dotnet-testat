﻿using AutoReservation.Dal;
using AutoReservation.Dal.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace AutoReservation.BusinessLayer
{
    public class AutoReservationBusinessComponent
    {
        // Auto CRUD operations
        public List<Auto> GetAutos()
        {
            using (AutoReservationContext context = new AutoReservationContext())
            {
                return context.Autos.Include(a => a.Reservationen).ToList();
            }
        }

        public Auto GetAutoById(int id)
        {
            using (AutoReservationContext context = new AutoReservationContext())
            {
                return context.Autos.Include(a => a.Reservationen).SingleOrDefault(a => a.Id == id);
            }
        }

        public int InsertAuto(Auto auto)
        {
            using (AutoReservationContext context = new AutoReservationContext())
            {
                // context.Entry(auto).State = EntityState.Added;
                context.Autos.Add(auto);
                context.SaveChanges();
                return auto.Id;
            }
        }

        public void UpdateAuto(Auto auto)
        {
            using (AutoReservationContext context = new AutoReservationContext())
            {
                try
                {
                    context.Entry(auto).State = EntityState.Modified;
                    context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    throw CreateLocalOptimisticConcurrencyException(context, auto);
                }
            }
        }

        public void DeleteAuto(Auto auto)
        {
            using (AutoReservationContext context = new AutoReservationContext())
            {
                //context.Entry(auto).State = EntityState.Deleted;
                context.Autos.Attach(auto);
                context.Autos.Remove(auto);
                context.SaveChanges();
            }
        }

        // Kunde CRUD operations
        public List<Kunde> GetKunden()
        {
            using (AutoReservationContext context = new AutoReservationContext())
            {
                return context.Kunden.Include(k => k.Reservationen).ToList();
            }
        }

        public Kunde GetKundeById(int id)
        {
            using (AutoReservationContext context = new AutoReservationContext())
            {
                return context.Kunden.Include(k => k.Reservationen).SingleOrDefault(k => k.Id == id);
            }
        }

        public int InsertKunde(Kunde kunde)
        {
            using (AutoReservationContext context = new AutoReservationContext())
            {
                context.Kunden.Add(kunde);
                context.SaveChanges();
                return kunde.Id;
            }
        }

        public void UpdateKunde(Kunde kunde)
        {
            using (AutoReservationContext context = new AutoReservationContext())
            {
                try
                {
                    context.Entry(kunde).State = EntityState.Modified;
                    context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                   throw CreateLocalOptimisticConcurrencyException(context, kunde);
                }
            }
        }

        public void DeleteKunde(Kunde kunde)
        {
            using (AutoReservationContext context = new AutoReservationContext())
            {
                context.Entry(kunde).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }

        // Reservation CRUD operations
        public List<Reservation> GetReservationen()
        {
            using (AutoReservationContext context = new AutoReservationContext())
            {
                return context.Reservationen.Include(r => r.Auto).Include(r => r.Kunde).ToList();
            }
        }

        public Reservation GetReservationById(int id)
        {
            using (AutoReservationContext context = new AutoReservationContext())
            {
                return context.Reservationen.Include(r=>r.Auto).Include(r => r.Kunde).SingleOrDefault(r => r.ReservationsNr == id);
            }
        }

        public int InsertReservation(Reservation reservation)
        {
            using (AutoReservationContext context = new AutoReservationContext())
            {
                context.Reservationen.Add(reservation);
                context.SaveChanges();
                return reservation.ReservationsNr;
            }
        }

        public void UpdateReservation(Reservation reservation)
        {
            using (AutoReservationContext context = new AutoReservationContext())
            {
                try
                {
                    context.Entry(reservation).State = EntityState.Modified;
                    context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    throw CreateLocalOptimisticConcurrencyException(context, reservation);
                }
            }
        }

        public void DeleteReservation(Reservation reservation)
        {
            using (AutoReservationContext context = new AutoReservationContext())
            {
                context.Entry(reservation).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }

        private static LocalOptimisticConcurrencyException<T> CreateLocalOptimisticConcurrencyException<T>(AutoReservationContext context, T entity)
            where T : class
        {
            var dbEntity = (T)context.Entry(entity)
                .GetDatabaseValues()
                .ToObject();

            return new LocalOptimisticConcurrencyException<T>($"Update {typeof(T).Name}: Concurrency-Fehler", dbEntity);
        }
    }
}